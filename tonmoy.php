<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Varriable handling function.</title>
    </head>
    <body>
        <h1>empty</h1>
        <?php
        $a = '10';
        $b = "";
        $c = NULL;
        
        echo $a;
        echo "</br>";
        if (empty($b)) {
            echo "This is emtpy";
        }
        else {
            echo "This is not emtpy";
        }
        echo $c;
        ?>
        <h1>gettype</h1>
        <?php
        $data = '10';
        $tonmoy = $data;
        echo $tonmoy;
        echo "</br>";
        echo gettype($tonmoy);
        
        ?>
        <h1>is_array</h1>
        <?php
        $arr1 = array('a', 'b', '', 'Robi');
        $arr2 = array('friend'=>'RIPON', 'family'=>'FATHER');
        $arr3 = array(array('This', 'is', 'an', 'array'), 'Hi', 'T' => 'Tonmoy', 'Guys');
        
        if (is_array($arr3)){
            echo "<pre>";
            print_r ($arr3);
            echo "<pre>";
        }
        else{
            echo "This is not an array";
        }
        ?>
        <h1>is_int</h1>
        <?php
        $d = 10;
        $e = '10';
        
        if (is_int($e)) {
            echo $e;
            echo "</br>";
            echo gettype($e);
        }
        else {
            echo "OOPS THIS IS NOT AN INTEGER";
        }
        ?>
        <h1>is_null</h1>
        <?php 
        $x = NULL;
        
        if (is_null($x)){
            echo $x;
            echo "</br>";
            echo "This is a NULL value";
        }
        else  {
            echo "This is not a NULL VALUE";
        }

        ?>
        <?php
        $n = '10';
        
        echo $n;
        echo gettype($n);
        ?>
    </body>
</html>

